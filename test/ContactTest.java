// Zoe and Mariele
import static org.junit.Assert.*;

import org.junit.Test;

public class ContactTest {

	// Test the constructor of the Contact class. 
	Contact contact1 = new Contact("Lauryn Hill", "5107779123");
	Contact contact2 = new Contact("Adele", "5105459123");
	Contact contact3 = new Contact("Ana Tijoux", "5107778823");
	Contact contact4 = new Contact("Princess Nokia", "5106469123");
	Contact contact5 = new Contact("Tarica June", "5107709123");
	Contact contact6 = new Contact("Sia", "5107109123");
	//Test the constructor of Contact with all parameters
	Contact contact7 = new Contact("Alicia Keys", "9179227723", 
			"411 East 14th New York NY 90020", "akeys@contact.com", "9172390040", "9175929009");
	
	//Test getName()
	@Test
	public void testGetName() {
		assertTrue(contact1.getName().equals("Lauryn Hill"));
		assertTrue(contact2.getName().equals("Adele"));
		assertTrue(contact3.getName().equals("Ana Tijoux"));
		assertTrue(contact4.getName().equals("Princess Nokia"));
		assertTrue(contact5.getName().equals("Tarica June"));
		assertTrue(contact6.getName().equals("Sia"));
		assertTrue(contact7.getName().equals("Alicia Keys"));
	}
	
	//Test getNumber()
		@Test
		public void testGetNumber() {
			assertTrue(contact1.getNumber().equals("5107779123"));
			assertTrue(contact2.getNumber().equals("5105459123"));
			assertTrue(contact3.getNumber().equals("5107778823"));
			assertTrue(contact4.getNumber().equals("5106469123"));
			assertTrue(contact5.getNumber().equals("5107709123"));
			assertTrue(contact6.getNumber().equals("5107109123"));
			assertTrue(contact7.getNumber().equals("9179227723"));
		}
		
		//Contact contact7 = new Contact("Alicia Keys", "9179227723", 
		//"411 East 14th New York NY 90020", "akeys@contact.com", "9172390040", "9175929009");
		//Test addAddress
		
		 @Test
	
		public void testAddAddress() {
			contact7.addAddress("411 East 14th New York NY 90020");
			assertEquals("failure - strings are not equal", contact7.address, "411 East 14th New York NY 90020");
		}
	    
		
		//Test addEmail
	    public void testAddEmail(){
	    	contact7.addEmail("akeys@contact.com");
			assertEquals("failure - strings are not equal", contact7.email, "akeys@contact.com");
	    }
	    
	    //Test addHomePhone
	    public void testAddHomePhone(){
	    	contact7.addHomePhone("9172390040");
			assertEquals("failure - strings are not equal", contact7.homePhone, "9172390040");
	    }
	    
	    //Test addTestAddWorkPhone
	    public void testAddWorkPhone(){
	    	contact7.addWorkPhone("9175929009");
			assertEquals("failure - strings are not equal", contact7.workPhone, "9175929009");
	    }

	    @Test
		public void testToString() {
			contact1.toString();
			contact2.toString();
			contact3.toString();
			contact4.toString();
			contact5.toString();
			contact6.toString();
			contact7.toString();
		}
}
