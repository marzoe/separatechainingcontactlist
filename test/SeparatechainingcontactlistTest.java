import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author Zoe, Mariele
 */
public class SeparatechainingcontactlistTest {
    
    public SeparatechainingcontactlistTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    //instance variables

    String kN = "3235752290";
    String k1N = "5102338394";
    String k2N = "5102438394";
    String k3N = "5102538394";
    String k4N = "5102638394";
    String k5N = "5102738395";

    //use to test methods other than insert to avoid duplication
    //helper method to create and populate a Separatechainingcontactlist object
    public Separatechainingcontactlist testObject(){
        Separatechainingcontactlist hTest = new Separatechainingcontactlist();
        hTest.insert("Katniss Everdeen", kN);
        hTest.insert("Angelina Johnson", k1N);
        hTest.insert("Nicki Bell", k2N);
        hTest.insert("Dominique Castillo", k3N);
        hTest.insert("Catalina Ramirez", k4N);
        
        return hTest;
    }
    
    /**
     * Test of contains method, of class Separatechainingcontactlist.
     */
    @Test
    public void testContains() {
        System.out.println("contains");
        Separatechainingcontactlist instance = testObject();
        
        String searchItem = "3235752290";
        boolean result = instance.contains(searchItem);
        assertEquals(true,result);
        
        String searchItem2 = "Katniss Everdeen";
        boolean result2 = instance.contains(searchItem2);
        assertEquals(true,result2);
        
        String searchItem3 = "32357522904444";
        boolean result3 = instance.contains(searchItem3);
        assertEquals(false,result3);
        
        String searchItem4 = "Lisa Simpson";
        boolean result4 = instance.contains(searchItem4);
        assertEquals(false,result4);
        
        String searchItem5 = k3N;
        boolean result5 = instance.contains(searchItem5);
        assertEquals(true,result5);
        
        String searchItem6 = "Dominique Castillo";
        boolean result6 = instance.contains(searchItem6);
        assertEquals(true,result6);
        
        assertEquals(instance.getNumContacts(),5);
    }
    
    /**
     * Mariele's test case
     */
    // SHould this be @Before instead of @Test? 
    @Test
    public void testInsert() {
        Separatechainingcontactlist sTest = new Separatechainingcontactlist();
	
        System.out.println("Test adding to map");
        
        /*Set up list of contacts
        Contact aC = new Contact("Katniss Everdeen", kN);
        Contact aC1 = new Contact("Angelina Johnson", k1N);
        Contact aC2 = new Contact("Angelina Johnson", k1N);
        Contact aC3 = new Contact("Angelina Johnson", k1N);
        Contact aC4 = new Contact("Angelina Johnson", k1N);
        */
        
        //Separatechainingcontactlist instance = new Separatechainingcontactlist();
        sTest.insert("Katniss Everdeen", kN);
        sTest.insert("Angelina Johnson", k1N);
        sTest.insert("Nicki Bell", k2N);
        sTest.insert("Dominique Castillo", k3N);
        sTest.insert("Catalina Ramirez", k4N);
       
        //Check insert() by checking that the contacts just inserted
        //are in the hash table.
        //Contains has been previously tested
        
        // Check for Contact Catalina Ramirez, k4N
        assertTrue(sTest.contains("Catalina Ramirez"));
        assertTrue(sTest.contains(k4N));
        
        //Check for Contact Katniss Everdeen, kN
        assertTrue(sTest.contains("Katniss Everdeen"));
        assertTrue(sTest.contains(kN));
        
        //Check for Angelina Johnson, k1N
        assertTrue(sTest.contains("Angelina Johnson"));
        assertTrue(sTest.contains(k1N));
        
        //Check for Nicki Bell, k2N
        assertTrue(sTest.contains("Nicki Bell"));
        assertTrue(sTest.contains(k2N));
        
        //Check for Dominique Castillo, k3N
        assertTrue(sTest.contains("Dominique Castillo"));
        assertTrue(sTest.contains(k3N));
    }

    
    
    @Test
    public void testGetNumContacts(){
        Separatechainingcontactlist instance = testObject();
        assertEquals(instance.getNumContacts(),5);
    }

    /**
     * Test of emptyMap method, of class Separatechainingcontactlist.
     */
    @Test
    public void testEmptyMap() {
        System.out.println("emptyMap");
        Separatechainingcontactlist instance = testObject();
        
        assertEquals(instance.getNumContacts(),5);
        assertEquals(instance.contains("Katniss Everdeen"),true);
        instance.emptyMap();
        
        assertEquals(instance.getNumContacts(),0);
    }


     /**
     * Test of find method, of class Separatechainingcontactlist.
     */
    @Test
    // Test find. Map is empty so should come back false.
    public void testFind() {
        Separatechainingcontactlist sTest = testObject();
        
        String expResult = k3N;
        String result = sTest.find("Dominique Castillo");
        assertEquals(expResult, result);
        
        String expResult2 = "Dominique Castillo";
        String result2 = sTest.find(k3N);
        assertEquals(expResult2, result2);
    }

    //use to test methods other than insert to avoid duplication
    //helper method to create and populate a small Separatechainingcontactlist object to test rehash
    public Separatechainingcontactlist testRehashObject(){
        //inserting one more object should cause a rehash
        Separatechainingcontactlist sTest = new Separatechainingcontactlist(6);
        sTest.insert("Katniss Everdeen", kN);
        sTest.insert("Angelina Johnson", k1N);
        sTest.insert("Nicki Bell", k2N);
        sTest.insert("Dominique Castillo", k3N);

        return sTest;
    }
    
    /**
     * Test of rehash method, of class Separatechainingcontactlist.
     */
    @Test
    // Test find. Map is empty so should come back false.
    public void testRehash() {
        //get original size
        Separatechainingcontactlist sTest = testRehashObject();
        
        //check the capacity and number of contacts
        int origCapacity = sTest.getCapacity();
        
        //initial tests to verify expected contacts and capacity
        //Capacity should be 7 - nextPrime of 6, the capacity that the test object was created with (see testRehashObject)
        assertEquals(7,origCapacity);
        
        //This should cause one rehash without causing two, even if the hash function isn't the most efficient
        sTest.insert("Catalina Ramirez", k4N);
        sTest.insert("Catalina B Ramirez", "7899879898");
        sTest.insert("Catalina C Ramirez", "7899879855");
        sTest.insert("Catalina D Ramirez", "7899879844");
        sTest.insert("Catalina E Ramirez", "7899879833");
        sTest.insert("Catalina F Ramirez", "7899879822");
        sTest.insert("Catalina G Ramirez", "7899879811");
        sTest.insert("Catalina H Ramirez", "7899879866");
        sTest.insert("Catalina I Ramirez", "7899879879");
        sTest.insert("Catalina J Ramirez", "7899879870");
        sTest.insert("Catalina K Ramirez", "7899879874");
        
        //get expected capacity and number of contacts
        //sTest.nextPrime(2 * origSize) should be nextPrime(14) = 17
        int newCapacity = 17;
        int newNumContacts = 15;
        
        //check that the rehash happened and the new capacity is correct
        assertEquals(newCapacity,sTest.getCapacity());
        
        //We added 15 contacts, the maximum number possible without potentially triggering a second rehash
        assertEquals(newNumContacts,sTest.getNumContacts());
    }
    
  /**
     * Test of delete method, of class Separatechainingcontactlist.
     */
    @Test
    public void testDelete() {
        System.out.println("delete");
        Separatechainingcontactlist sTest = testObject();
        
        String input1 = "Katniss Everdeenee";
        boolean expResult = false;
        boolean result = sTest.delete(input1);
        assertEquals(expResult, result);
        
        assertTrue(sTest.contains("Dominique Castillo"));
        boolean del = sTest.delete("Dominique Castillo");
        //test the delete() method of Separatechainingcontactlist
        assertEquals(true,del);
        
        //Assert that Contact "Dominique Castillo", k3N has been deleted from both has tables. 
        assertEquals(false,sTest.contains(k3N));
        assertEquals(false,sTest.contains("Dominique Castillo"));
    }

        @Test
    public void testPrintAllContacts() {
    //reference: https://stackoverflow.com/questions/32241057/how-to-test-a-print-method-in-java-using-junit
    //Dakshinamurthy Karra
        Separatechainingcontactlist sTest = testObject();
        
        System.out.println("printAllContacts");
        sTest.printAllContacts();
        
        //Expected outcome
        //Katniss Everdeen=Name: Katniss Everdeen; Number: 3235752290; Address: null; Email: null; Home: null; Work: null
        //Angelina Johnson=Name: Angelina Johnson; Number: 5102338294; Address: null; Email: null; Home: null; Work: null
        //Dominique Castillo=Name: Dominique Castillo; Number: 5102538194; Address: null; Email: null; Home: null; Work: null
        //Catalina Ramirez=Name: Catalina Ramirez; Number: 5102638994; Address: null; Email: null; Home: null; Work: null
        //Nicki Bell=Name: Nicki Bell; Number: 5102438794; Address: null; Email: null; Home: null; Work: null

        //needs modification so it actually tests the print method
        //create a list of items that should be in the printed output for this test
        ArrayList cList = new ArrayList();
        
        //populate the list
        Contact c1 = new Contact("Katniss Everdeen", kN);
        Contact c2 = new Contact("Angelina Johnson", k1N);
        Contact c3 = new Contact("Nicki Bell", k2N);
        Contact c4 = new Contact("Dominique Castillo", k3N);
        Contact c5 = new Contact("Catalina Ramirez", k4N);
        
        cList.add(c1);
        cList.add(c2);
        cList.add(c3);
        cList.add(c4);
        cList.add(c5);
        
        //Save System.out
        PrintStream oldSysOut = System.out;
        
        //Use a ByteArrayOutputStream to get the print call output
        ByteArrayOutputStream thisBaos = new ByteArrayOutputStream();
        
        //Point System.out to the baos
        System.setOut(new PrintStream(thisBaos));     //commenting out so test class compiles
        
        //print the arraylist with the test data
        System.out.print(cList);
        
        //reset System.out
        System.setOut(oldSysOut);
        
        //thisBaos should have the print output
        String SysOutPrint = new String(thisBaos.toByteArray());
        
        //Assertions
        assertTrue(SysOutPrint.contains("Name: Katniss Everdeen"));
        assertTrue(SysOutPrint.contains("Name: Angelina Johnson"));
        assertTrue(SysOutPrint.contains("Name: Nicki Bell"));
        assertTrue(SysOutPrint.contains("Name: Dominique Castillo"));
        assertTrue(SysOutPrint.contains("Name: Catalina Ramirez"));
        assertTrue(SysOutPrint.contains("Number: " + kN));
        assertTrue(SysOutPrint.contains("Number: " + k1N));
        assertTrue(SysOutPrint.contains("Number: " + k2N));
        assertTrue(SysOutPrint.contains("Number: " + k3N));
        assertTrue(SysOutPrint.contains("Number: " + k4N));
    }
}

