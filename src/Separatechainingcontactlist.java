import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
//
// CS 124 Project - contact list based on separate chaining implementation of hash map
// Mariele, Zoe
//
// Necessary functions:
// Insert key, value pair by key or value
// Remove key, value pair by key or value
// Get key, value pair by key or value
//
//Implementation: HashMap that overflows into linkedlist
//

/**
 * 
 */
public class Separatechainingcontactlist<G> {
    //instance variables
    private List<Contact> [ ] listArrayNums;      //array of lists to serve as foundation of map - name keys
    private List<Contact> [ ] listArrayNames;     //array of lists to serve as foundation of map - number keys
    private int sizeNow = 0;                         //tracks current size of map; same as listArrayNums.length = listArrayNames.length
    
    
    //set a default table size for the constructor
    //this is an arbitrary number that is not too small or too large
    private static final int DEFAULT_TABLE_SIZE = 61;
    private int capacity;
    
    //constructor with specific capacity
    public Separatechainingcontactlist(int givenCapacity){
        //prime number for table creation
        int nextPrime = nextPrime(givenCapacity);
        
        //tracks number of buckets, only changes at creation and at rehash
        capacity = nextPrime;
        
        //create name and number tables
        listArrayNums = new LinkedList[nextPrime];
        listArrayNames = new LinkedList[nextPrime];
        
        //Populate name and number tables
        for(int i=0; i<listArrayNums.length; i++){
            listArrayNames[i] = new LinkedList();
            listArrayNums[i] = new LinkedList(); 
        }
    }
    
    //constructor with default capacity, uses constructor with specific capacity
    public Separatechainingcontactlist(){
        this( DEFAULT_TABLE_SIZE );
    }
    
    // Mariele added this
    // getNumContacts() because I need to access private instance variable sizeNow 
    //outside of the class to test.
    //All visibilities should probably be protected/package, whichever is more appropriate
    
    //Theta(1), just returns a variable
    public int getNumContacts() {
    	return sizeNow;
    }
    
    //Theta(1), just returns a variable
    public int getCapacity(){
        //returns current number of buckets. Used to test rehash.
        return capacity;
    }
    
    //Method to remove nonnumerical characters from phone number and validate phone number
    //Theta(number.length) because Java checks each char in the String
    public String processNumber(String number){
        number = number.replaceAll("[^\\d]", "");
        return number;
    }
    
    //Mariele
    // if myNum is 1, it is not prime. if myNum is 2 or 3, it is prime.
    //O(myNum) because the for loop counts up from 2 to myNum/2, so Theta(1) because myNum is a constant
    private static boolean isPrime(int myNum){
    	if (myNum == 1) {
    		return false;
    	}
    	if (myNum == 2 || myNum == 3) {
    		return true;
    	}
    	for (int i = 2; i < myNum/2; i ++) {
    		if( myNum % i == 0) {
            	return false;
            }
    	}
    	return true;
    }
            
            
    //Mariele  
    //Also Theta(1) for the same reason as isPrime
    private static int nextPrime(int myNum){
    	// if myNum is even, the next possible prime will be myNum + 1, 
    	// the next odd number
        if (myNum % 2 == 0) {
        	myNum++;
        }
        // if we get to here, number is odd. which means next possible prime
        // will be the next odd, that is, myNum + 2.
        while (!isPrime(myNum)) {
        	myNum +=2;
        }
        return myNum;
    }
    
    
    //Zoe
    //hash the name and number of a given contact to the same bucket,
    //so that the contact can be found by either name or number
    //can hash either a String or a String representing a number
    //This is not used, but is here as another possible hash method
    //Theta(key.length), because it iterates over the characters in the String input
    private int hash(String key, int sizeOfMap){
        //give the function a prime number
        int primeNo = 37;
        
        //create a value to store the new hashcode
        int hashValue = 0;
        
        //reproduce the Java hashcode: modify the value of each character in the String
        //representation of key by a prime number
        //text uses 37 for prime number
        //hashes using the default Java method with a specified prime number
        for(int i=0; i<key.length(); i++){
            hashValue = primeNo * hashValue + key.charAt(i);
            
            hashValue %= sizeOfMap;
            
            if(hashValue < 0){
                hashValue += sizeOfMap;
            }
        }
        return hashValue;
    }
    
    //Zoe
    //Needed when inserting causes the table size to need to be increased
    //This is O(n) with a good hash function, but could be more complicated with a bad hash function
    //The most complex part is lines 166-170. With linked lists of the ideal length (length=1)
    //this part of the function will run in O(n) time where n is the number of linked lists in the array
    //With longer lists, it will run in O(n*i) time, where n is as above and i refers to the length of the linked lists
    //This should still be O(n) time, but could practically be worse than that if the lists are very long. A very full, very inefficient
    //map where the lists were the length of the size of the map would effectively not run in O(n) time, but closer to O(n^2). The likelihood
    //of that case is very small.
    private void rehash(){
        //Create temporary placeholder list arrays
        List<Contact> [] oldListArrayNums = new LinkedList[listArrayNums.length];
        List<Contact> [] oldListArrayNames = new LinkedList[listArrayNames.length];
        
        //add lists to the new arrays
        for(int i=0; i<oldListArrayNums.length; i++){
            oldListArrayNums[i] = listArrayNums[i];
            oldListArrayNames[i] = listArrayNames[i];
        }
        
        //Double the size of both underlying arrays
        int currentNextPrime = nextPrime(2 * listArrayNums.length);
        listArrayNums = new List[currentNextPrime];
        listArrayNames = new List[currentNextPrime];
        
        //update the capacity variable
        capacity = currentNextPrime;
        
        //could check the size against the previous size
        int oldSize = sizeNow;
        
        //reset size because inserting will re-update the size
        sizeNow = 0;
        
        //add lists to the new arrays
        for(int i=0; i<listArrayNums.length; i++){
            listArrayNums[i] = new LinkedList<>();
            listArrayNames[i] = new LinkedList<>();
        }
        
        //reinsert all entries to the new larger table so they hash correctly
        for(List<Contact> listNums:oldListArrayNums){
            for(Contact num:listNums){
                //insert the key and value into the new table
                insert(num.name,num.number);
            }
        }
    }
         
    //Zoe
    //this method is used to implement the remove method and uses hashcode of the key being hashed
    //Theta(1), this just performs some calculations
    private int thisHash(String keyToHash){
        //get the Java hashcode of the item being hashed
        int hashValue = keyToHash.hashCode();
        
        //fit the hash code to the number of available buckets
        hashValue %= listArrayNums.length;
        
        //wrap around to the high end of the map indices if the fitted hash value is too small
        if( hashValue < 0){
            hashValue += listArrayNums.length;
        }
        
        return hashValue;
    }
    
    //Zoe
    //removes all k,v pairs from the map
    //Theta(n) where n is the size of the array of lists, which is the size of the map
    public void emptyMap(){
        //clear all existing list
        for (int i=0; i<listArrayNums.length; i++){
            listArrayNums[i].clear();
            listArrayNames[i].clear();
        }
        sizeNow = 0;
    }
    
    //Zoe
    //Insert method: no duplicates, no null, 1 name per number
    //do nothing if the item to insert is already present,
    //otherwise add it to the appropriate list in the appropriate array of linkedlists
    
    //Theta(1), This just accesses linked lists and adds nodes (Contacts)
    //Rehashing increases the complexity to Theta(n)
    public boolean insert(String name, String number){  //why is this boolean??
        //add the pair to insert to both maps with the appropriate item as key
        
        String pNum = processNumber(number);
        
        //Make a Contact object to insert as the value
        Contact aContact = new Contact(name, pNum);
        
        //hash the appropriate key to figure out which bucket to add the k,v pair to
        //get the index of the list where the item should be stored
        List numberList = listArrayNums[thisHash(pNum)];
        List nameList = listArrayNames[thisHash(name)];
        
        //if the number list contains the item as a key, so does the name list, so
        //append the item to the appropriate linked list in both arrays
        if(!numberList.contains(name)){
            //doesn't do anything if duplicate
            //add the appropriate k,v pair to the appropriate list in both tables
            numberList.add(aContact);
            nameList.add(aContact);
            sizeNow++;
        }
        
        //rehash if necessary
        if(sizeNow+1 > listArrayNums.length){
            rehash();
        }
        
        return true;
    }
    
   //Mariele
    //Get method: return name or return number eg key or value?
    //Theta(n), where n is the length of the longest list
    //This should be Theta(1) if the hash function is placing items efficiently.
    //The current hash function does sometimes have collisions, especially on maps < ~23 in length
    //based on testing different array sizes
    public String find(String nameORnumber) {
            //get the bucket
            int position = thisHash(nameORnumber);
            
            Iterator<Contact> nameIter = listArrayNames[position].iterator();
            while(nameIter.hasNext()){
                Contact eachContact = nameIter.next();
                if (nameORnumber.equals(eachContact.name)){
                    return eachContact.number;
                }
            }
            
            Iterator<Contact> numIter = listArrayNums[position].iterator();
            while(numIter.hasNext()){
                Contact someContact = numIter.next();
                if (nameORnumber.equals(someContact.number)){
                    return someContact.name;
                }
            }
        return "Not found";
    }
    
    //Mariele
    //Delete method: remove k,v pair from map by getting it by name or number
    //Theta(n), where n is the length of the longest list
    //so should be Theta(1) if the hash function is placing items efficiently.
    //The regex matching adds some complexity, but it is only on the order of the length of the String
    //being tested
    public boolean delete(String nameORnumber) {
        //Set up test condition to see if data is numeric
        String regexTest = "\\d+";
        
        //Iterator, Contact and String variables to store iterators, retrieved contacts and 
        //saved data corresponding to the data provided to delete
        Contact someContact, eachContact;
        String theName = "", theNumber = "";
        Iterator<Contact> numIter, nameIter;
        int position, newPosition;
        
        //get the bucket
        position = thisHash(nameORnumber);
        
        //if you have a fully numeric String, a phone number
        if(nameORnumber.matches(regexTest)){
            //search the number list first
            numIter = listArrayNums[position].iterator();
            while(numIter.hasNext()){
                someContact = numIter.next();
                if (someContact.number.equals(nameORnumber)){
                    //save the name that corresponds
                    theName = someContact.name;
                    numIter.remove();
                }
            }
            
            //get position of name to remove
            newPosition = thisHash(theName);
            
            //now remove the name from that list
            nameIter = listArrayNames[newPosition].iterator();
            while(nameIter.hasNext()){
                eachContact = nameIter.next();
                if (eachContact.name.equals(theName)){
                    nameIter.remove();
                    sizeNow--;
                    return true;
                }
            }
            
        //if you have a name instead
        } else {
            //search the name list first
            nameIter = listArrayNames[position].iterator();
            while(nameIter.hasNext()){
                someContact = nameIter.next();
                if (someContact.name.equals(nameORnumber)){
                    //save the number that corresponds
                    theNumber = someContact.number;
                    nameIter.remove();
                }
            }
            
            //get bucket of number key to remove
            newPosition = thisHash(theNumber);
            
            //now remove the number from that list
            numIter = listArrayNums[newPosition].iterator();
            while(numIter.hasNext()){
                eachContact = numIter.next();
                if (eachContact.number.equals(theNumber)){
                    numIter.remove();
                    sizeNow--;
                    return true;
                }
            }
        }
        return false;
    }
    
    //Zoe
    //Theta(n), where n is the length of the longest list
    //This should be Theta(1) if the hash function is placing items efficiently.
    public boolean contains(String searchItem){
        //get the bucket
        int position = thisHash(searchItem);

        Iterator<Contact> nameIter = listArrayNames[position].iterator();
        Contact eachContact;
        String contactName;
        
        while(nameIter.hasNext()){
            eachContact = nameIter.next();
            contactName = eachContact.name;
            if (contactName.equals(searchItem)){
                return true;
            }
        }

        Iterator<Contact> numIter = listArrayNums[position].iterator();
        Contact someContact;
        String contactNum;
        
        while(numIter.hasNext()){
            someContact = numIter.next();
            contactNum = someContact.number;
            if (contactNum.equals(searchItem)){
                return true;
            }
        }
        return false;
    }


    //Zoe
    //Iterate through the table and print every available k,v pair
    //prints from name
    //Theta(n), where n is the size of the map, because it has to iterate through the entire
    //array of lists
    public void printAllContacts(){
        //add space for formatting
        System.out.println("Map contents");
        
        //print each list in the array of lists using their toString method
        for(List<Contact> item:listArrayNames){
            if(!item.isEmpty()) { System.out.println(item.toString()); }
        }
        
        //add space for formatting
        System.out.println();
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //additional testing
        
        /*the insert method handles names and numbers, but the print all method handles full contacts and the
        contact toString method prints the full contact. Changing the contact toString method or the insert method to support either a full information implementation
        or a limited name and number implementation would make our solution better-presented.
        */
        //Create an instance of the object - test the constructor
        Separatechainingcontactlist thisMap = new Separatechainingcontactlist();
        
        //Populate the object - test insert, including insertion of phone numbers with invalid characters
        thisMap.insert("Lisa Simpson", "000111-----%%%%4444$$");
        thisMap.insert("Bart Simpson", "1112223333");
        thisMap.insert("Maggie Simpson", "5555555555");
        thisMap.insert("Homer Simpson", "1231231234");
        thisMap.insert("Grandpa Simpson", "8888*****55533&&&&4");
        thisMap.insert("Liz Lemon", "111222ffhhjj3443!");
        thisMap.insert("Jon Snow", "8882223333");
        thisMap.insert("Arya Stark", "4445556666");
        
        //print all of the contacts to show that they were added
        thisMap.printAllContacts();
        
        //Use the size/print number of contacts method to verify that the number of contacts stored in the map is correct
        //There should be 8 contacts stored.
        System.out.println("Number of contacts: " + thisMap.getNumContacts() + "\n");
        
        //delete a few contacts by both name and number
        thisMap.delete("Jon Snow");
        thisMap.delete("5555555555");
        thisMap.delete("8888555334");
        
        //print all of the contacts to show that there were deletions
        thisMap.printAllContacts();
        
        //Use the size/print number of contacts method to verify that the number of contacts stored in the map is correct
        //after the deletions. There should be 5 contacts stored.
        System.out.println("Number of contacts: " + thisMap.getNumContacts() + "\n");
        
        //Find a few of the contacts by name and number, including one that doesn't exist
        System.out.println(thisMap.find("Lisa Simpson"));
        System.out.println(thisMap.find("0001114444"));
        System.out.println(thisMap.find("5555555555"));
        System.out.println(thisMap.find("0000000000"));
        
        //Verify with size and find that it is cleared
        System.out.println("\nNumber of contacts: " + thisMap.getNumContacts());
        
        //Empty the map
        thisMap.emptyMap();
        
        //Verify with size and find that it is cleared
        System.out.println("\nNumber of contacts: " + thisMap.getNumContacts());
    }
}
